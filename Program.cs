﻿using System;
using System.IO;


namespace ChangeExtension
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = Environment.CurrentDirectory;
            var appname = "";
            var extension = "";
            try
            {
                appname = args[0];
                extension = args[1];
            } catch
            {
                Console.WriteLine("Bad usage, use : ce appname extension");
                Environment.Exit(0);
            }

            if(appname.Contains("."))
            {
                Console.WriteLine("Bad usage, use : ce appname extension");
                Environment.Exit(0);
            }
            if(File.Exists($"{path}\\{appname}"))
            {
                File.Move($"{path}\\{appname}", $"{path}\\{appname.Split(".".ToCharArray())[0].Trim()}.{extension}");
            } else
            {
                Console.WriteLine($"Unable to found file {path}\\{appname}");
            }
        }
    }
}


